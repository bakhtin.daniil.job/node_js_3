const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');

const {parsed: {SECRET_KEY}} = require('dotenv').config({path: './.env'});

const authMiddleware = async(req, res, next) => {
    const  {authorization} = req.headers;
    
      if (!authorization) {
        return res.status(401).json({ 'message': 'Please, provide authorization header' });
      }
    
      const [, token] = authorization.split(' ');
    
      if (!token) {
        return res.status(401).json({ 'message': 'Please, include token to request' });
      }    
    try {   
        const tokenPayload = jwt.verify(token, SECRET_KEY)

        const currentProfile = await User.findById(tokenPayload._id);

        if(!currentProfile){
            return res.status(400).json({massage: 'User profile is not defined'})
        }

        req.currentProfile = tokenPayload;

        next();

    } catch(e){
        return res.status(400).send({
            'massage': err.massage
        })
    }
}

module.exports = {
    authMiddleware
};
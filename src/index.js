const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const fs = require('fs');
const path = require('path');

const { authRouter } = require('./router/authRouter');
const { userRouter } = require('./router/userRouter');
const { truckRouter } = require('./router/truckRouter')
const { loadRouter } = require('./router/loadRouter')
const { weatherRouter } = require('./router/weatherRouter')

const { authMiddleware } = require('./middlewares/authMiddleware');


const {parsed: {PORT, MOONGO_KEY}} = require('dotenv').config({path: './.env'})
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})

mongoose.connect(MOONGO_KEY);

app.use(express.json());
app.use(morgan('tiny', { stream : accessLogStream}));

app.use('/api/auth', authRouter);
app.use('/api/weather', weatherRouter)
app.use('/api/users/me', authMiddleware, userRouter);
app.use('/api/trucks', authMiddleware, truckRouter);
app.use('/api/loads', authMiddleware, loadRouter);
const start = async () => {
    try {
      app.listen(PORT || 8080);
    } catch (err) {
      console.error(`Error on server startup: ${err.message}`);
    }
  };
  
  start();
  
  app.use(errorHandler);
  
  function errorHandler(err, req, res, next) {
    console.log(err.message);
    res.status(500).send({message: 'Server error'});
  }
  
  
const express = require('express');

const { getUserInfo, deleteUserInfo, updatePassword } = require('../controllers/userController');

const router = express.Router();

router
    .get('', getUserInfo)
    .delete('', deleteUserInfo);


router.patch('/password', updatePassword)


module.exports = {
    userRouter: router
}
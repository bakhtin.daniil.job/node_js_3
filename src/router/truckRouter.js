const express = require('express');

const {  getTrucks, createTruck, getTruckById, updateTruckById, deleteTruck, assignedTruck } = require('../controllers/truckController');

const router = express.Router();

router
.get('', getTrucks)
.post('', createTruck);

router
.get('/:id', getTruckById)
.put('/:id', updateTruckById)
.delete('/:id', deleteTruck);

router.post('/:id/assign', assignedTruck);

module.exports = {
    truckRouter: router
}
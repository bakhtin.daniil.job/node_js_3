const express = require('express');

const { showWeatherInfo } = require('../controllers/weatherController');

const router = express.Router();

router.get('', showWeatherInfo)


module.exports = {
    weatherRouter: router
}
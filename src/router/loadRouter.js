const express = require('express')
const {  
    getLoads, 
    createLoad, 
    getActiveLoads, 
    updateLoadActive, 
    getLoadById, 
    updateLoadById, 
    deleteLoadById, 
    getShippingInfo, 
    createLoadById 
} = require('../controllers/loadController')

const router = express.Router();

router
.get('/', getLoads)
.post('/', createLoad)
.get('/active', getActiveLoads)
.patch('/active/state', updateLoadActive)
.get('/:id', getLoadById)
.put('/:id', updateLoadById)
.delete('/:id', deleteLoadById)
.post('/:id/post', createLoadById)
.get('/:id/shipping_info', getShippingInfo)


module.exports = {
    loadRouter: router
}
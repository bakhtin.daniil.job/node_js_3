const express = require('express');
const { reqisterUser, loginUser, forgotPassUser } = require('../controllers/authController');

const router = express.Router();

router.post('/register', reqisterUser);

router.post('/login', loginUser);

router.post('/forgot_password', forgotPassUser)


module.exports = {
    authRouter: router
}
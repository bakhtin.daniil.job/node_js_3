const weather = require('weather-js');


const showWeatherInfo = async (req, res, next) => {
    try {
    weather.find({search: 'Kyiv, UA', degreeType: 'C'}, function(err, result) {
        if(err) console.log(err);
      
        res.status(200).json({
            result
        })
      });
    } catch (err) {
        return res.status(500).json({
            message: err.message
        })
    }
}


module.exports = {
    showWeatherInfo
}
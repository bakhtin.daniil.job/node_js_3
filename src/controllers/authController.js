const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const uuid = require("uuid");
const sendMail = require("@sendgrid/mail");



const {parsed: {ENCODE_ROUNDS, SECRET_KEY, MY_SECRET_EMAIL, SENDGRID_TEMPLATE_ID}} = require('dotenv').config({path: './.env'});

const { User, userValidate, loginValidation, emailValidation} = require('../models/userModel');

const reqisterUser = (req, res, next) => {
    const { email, password, role } = req.body;
    try{
    const { error } = userValidate(req.body)
        if(error) {
            res.status(400).json({
                message: error.details.message
            })
        }

    if (!email) {
      return res.status(400).send({
        message: 'Field username is empty',
      });
    }
  
    if (!password) {
      return res.status(400).send({
        message: 'Field password is empty',
      });
    }

    if (!role) {
        return res.status(400).send({
            message: 'Field role is empty',
          });
    }
    
    const user = new User({
      email: email,
      password: bcrypt.hashSync(password, bcrypt.genSaltSync(+ENCODE_ROUNDS)),
      role: role
    });
    
    user
    .save()
    .then((respon) => {
        console.log(respon)
        res.status(200).send({ 'message': 'Success' }); 
    })
    .catch(err => console.error(err))
} catch (err) {
    return res.status(500).json({message: err.message});
}
}
const loginUser = async (req, res, next) => {
    const { email, password } = req.body;
    try {
        const { error } = loginValidation(req.body);

        if(error){
            res.status(400).json({
                message: error.details[0].message
            })
        }

        if (!email) {
            return res.status(400).send({
            message: 'Incorrect email',
            });
        }

        if (!password) {
            return res.status(400).send({
            message: 'Incorrect password',
            });
        }

        const current = await User.findOne({email: email});
        if (bcrypt.compareSync(password, current.password)) {
      const token = jwt.sign(
          {
            _id: current._id,
            email,
            role: current.role,
            createDate: current.createDate,
          },
          SECRET_KEY,
          {expiresIn: '30m'},
      );
      return res.status(200).json({
        message: 'Success',
        jwt_token: token,
      });
    }

    return res.status(400).json({
      message: 'Passwords do not match',
    });
  } catch (err) {
    console.log(err)
    return res.status(500).json({message: 'Not authorized'});
  }




}

const forgotPassUser = async (req, res, next) => {
    const { email } = req.body;
    try {
      // sendMail.setApiKey(process.env.SENDGRID_API_KEY);
        const { error } = emailValidation(req.body);

        if(error){
            res.status(400).json({
                message: error.details[0].message
            })
        }

        if(!email){
            return res.status(400).send({
                message: 'Incorrect email',
                });
        }
        if(!error){
        const current = await User.findOne({email: email});
        if(current){
          const newPassword = uuid.v4();
          const encryptedPassword = bcrypt.hashSync(
            newPassword,
            bcrypt.genSaltSync(+ENCODE_ROUNDS)
          );
          currentUser.password = encryptedPassword;
        
          console.log(newPassword);
        
          await currentUser.save();
         
          await sendMail.send({
            to: {
              email
            },
            from: {
              email: MY_SECRET_EMAIL
            },
            templateId: SENDGRID_TEMPLATE_ID,
            dynamicTemplateData: {
              email,
              password
            }
          });
      
        return res.status(200).json({
            message: "New password sent to your email address"
        })
        } else {
          return res.status(400).send({
            message: 'Incorrect email',
            });
        }
    }
    } catch (err) {
        console.log(err)
        return res.status(500).json({message: 'Not autorized'});
    }
}


module.exports = {
    reqisterUser,
    loginUser,
    forgotPassUser
}

const { Load, loadValidation } = require('../models/loadModel');
const { Truck } = require('../models/truckModel');
const { loadIsFit } = require('../helper/truckFit');

const getLoads = async (req, res, next) => {
    try {
    const { _id, role } = req.currentProfile;
    const { status } = req.query;
    const queryObj = {};
    const offset = +req.query.offset || 0;
    let limit = +req.query.limit;

    if(status){
        queryObj.status = status;
    }

    if (limit < 10 || isNaN(limit)) {
        limit = 10;
    } else if (limit > 50) {
        return res.status(400).json({
        message: 'Limit cannot be more than 50',
        });
    }

    if(role === 'SHIPPER'){
        queryObj.created_by = _id;
        const loads = await Load.find(queryObj).select(['-__v']).skip(offset).limit(limit)
        return res.status(200).json({ count: loads.length, loads})
    }
    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}

const createLoad = async (req, res, next) => {
    try {
        const { _id, role } = req.currentProfile;
        const { error } = loadValidation(req.body);
        const {name, payload, pickup_address, delivery_address, dimensions} = req.body;

        if(error){
            res.status(403).json({
                message: error.details[0].message
            })
        }

        if(role === 'DRIVER'){
            res.status(400).json({
                message: "The loads create can only shipper"
        })
        }

        if(!error) {
            const load = new Load({
                created_by: _id,
                assigned_to: _id,
                name,
                payload,
                pickup_address,
                delivery_address,
                dimensions
            })

            await load
            .save()
            .then(ress => {
                console.log(ress)
                res.status(200).json({
                    message: "Load created successfully"
                })
            })
            .catch(err => {
                res.status(400).json({
                    message: err.message
                })
            })
        }



    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}


const getActiveLoads = async (req, res, next) => {
    try {
        const { _id, role } = req.currentProfile;

        if(role.toLowerCase() === 'driver'){
            const activeLoads = await Load.findOne({ assigned_to: _id }).select(['-__v'])

            res.status(200).json({ load: activeLoads })
        } else {
            res.status(400).json({ message: 'Denied' });
          }      
       
    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}

const updateLoadActive = async (req, res, next) => {
    try {
        const { _id, role } = req.currentProfile;

        if(role.toLowerCase() === 'driver'){
            let states = ['En route to Pick Up', 'En route to delivery', 'Arrived to delivery'];

            const activeLoad = await Load.findOne({ assigned_to: _id })

            if(!activeLoad){
                return res.status(400).send({ "message": `Assigned load not found` })
            }

            if((await activeLoad.state) === states[0]){
                await Load.findOneAndUpdate({assigned_to: _id}, { $set: { state: states[1]}})

                await activeLoad.save()

                res.status(200).json({
                    message: `Load state changed to ${states[1]}`
                })
            }

            

            if((await activeLoad.state) === states[1]){
                await Load.findOneAndUpdate({assigned_to: _id}, {$set: {state: states[2], status: 'SHIPPED'}})

                await activeLoad.save()

                await Truck.findOneAndUpdate({ _id: _id, status: 'OL'}, { $set: { status: 'IS' }})

                res.status(200).json({
                    message: `Load state changed to ${states[2]}`
                })
            } else {
                return res.status(200).json({
                    message: "Load is already delivered"
                })
            }
          

        } else {
            res.status(400).json({ message: 'Access denied' });
        }

    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}

const getLoadById = async (req, res, next) => {
    try{ 
        const { id } = req.params;
        const { _id, role } = req.currentProfile;

        await Load.findById({ _id: id, created_by: _id}).then(ress => {
            res.status(200).json({ ress })
        })
    } catch (err) {
        res.status(500).json({
            message: err.message
        })  
    }
}

const updateLoadById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { _id, role } = req.currentProfile;
        const {name, payload, pickup_address, delivery_address, dimensions} = req.body;

        const currentLoad = await Load.findById({ _id: id, created_by: _id });

        if((await currentLoad.status) === "NEW"){
            await Load.findOneAndUpdate(
                { _id: id, created_by: _id },
                {
                    $set: {
                        name,
                        payload,
                        pickup_address,
                        delivery_address,
                        dimensions
                    }
                }
            ).then(ress => {
                res.status(200).json({
                    message: "Load details changed successfully"
                })
            })
        } else {
            res.status(400).json({
                message: 'Load details cannot be changed',
              });        
        }        
    } catch (err) {
        res.status(500).json({
            message: err.message
        })  
    }
}

const deleteLoadById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { _id, role } = req.currentProfile;

        const currentLoad = await Load.findById({ _id: id, created_by: _id });

        if((await currentLoad.status) === "NEW"){
            await Load.findOneAndDelete(id).then(() => {
                res.status(200).json({
                    message: 'Load deleted successfully'
                })
            })
        } else {
            res.status(400).json({
              message: 'Load cannot be deleted',
            });
          }      
    } catch (err) {
        res.status(500).json({
            message: err.message
        })  
    }
}

const getShippingInfo = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { _id, role } = req.currentProfile;

        const truckCurrent = await Truck.findOne({ status: 'OL' });

        const loadCurrent = await Load.findById(id);

        res.status(200).json({
            load: loadCurrent,
            truck: truckCurrent
        })

    } catch (err) {
        res.status(500).json({
            message: err.message
        })  
    }
}

const createLoadById = async (req, res, next) => {
    try {
        const { id } = req.params;

        const currentLoad = await Load.findById(id);
      
        if (!currentLoad) {
          return res.status(400).json({
            message: `Load with id: ${id} is not found`
          });
        }
        const activeTruck = await Truck.find({status: 'IS', assigned_to: { $ne: null }});
        const firstTruck = activeTruck.filter((truck) => loadIsFit(truck.type, currentLoad.dimensions, currentLoad.payload))[0];
        
          if (currentLoad.status.toLowerCase() === 'posted') {
            return res.status(400).json({ message: `Load with id: ${id} is already posted` });
          }
          
        if(!firstTruck){
            await Load.findByIdAndUpdate(
                { _id: id },
                {
                  $set: {
                    status: 'NEW',
                    logs: {
                      message: 'Driver was not found',
                      time: new Date().toISOString(),
                    },
                  },
                },
              );
              return res.status(400).json({
                message: `There is no found truck to deliver load '${currentLoad.name}'`
              });          
        }
      
        if(firstTruck){
            dataUpdate(firstTruck.assigned_to)
        }

        async function dataUpdate(idDriver) {
            await firstTruck.updateOne({
              $set: { status: 'OL' },
            });
            await Load.findByIdAndUpdate(
              { _id: id },
              {
                $set: {
                  status: 'ASSIGNED',
                  state: 'En route to Pick Up',
                  assigned_to: idDriver,
                  logs: {
                    message: `Load assigned to driver with id ${idDriver}`,
                    time: new Date().toISOString(),
                  },
                },
              },
            );
            return res.json({
              message: 'Load posted successfully',
              driver_found: true,
            });
          }
        

    } catch (err) {
        res.status(500).json({
            message: err.message
        })  
    }
}

module.exports = {
    getLoads,
    createLoad,
    getActiveLoads,
    updateLoadActive,
    getLoadById,
    updateLoadById,
    deleteLoadById,
    getShippingInfo,
    createLoadById
}
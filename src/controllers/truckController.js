const { Truck, truckValidation } = require('../models/truckModel');


const getTrucks = async (req, res, next) => {
    const { _id, role } = req.currentProfile;
    try{
        if(role.toLowerCase() === 'shipper'){
            res.status(400).json({
                message: "Shipper can't have trucks"
            })
        }
        const findTruck = await Truck.find({ created_by: _id }).select(['-__v', '-demension', '-payload'])
        res.status(200).json({
            trucks: findTruck,
            count: findTruck.length
        })

    } catch (err){
        res.status(500).json({
            message: err.message
        })
    }
}


const createTruck = async (req, res, next) => {
    try{
        let payload = 0;
        let demension = {};
        const { _id, role } = req.currentProfile;
        const { error } = truckValidation(req.body);
        const { type } = req.body;

        if( error ) {
            res.status(403).json({
                message: error.details[0].message
            })
        }

        if(role.toLowerCase() === 'shipper'){
            res.status(400).json({
                message: "Shipper can't have trucks"
            })
        }

        if(type.toLowerCase() === 'sprinter'){
            payload = 1700;
            demension = {
                width: 300,
                length: 250,
                height: 170,

            };
        } else if(type.toLowerCase() === 'small straight'){
            payload = 2500;
                demension = {
                    width: 500,
                    length: 250,
                    height: 170,
                };
        } else if(type.toLowerCase() === 'large straight'){
            payload = 4000;
                demension = {
                    width: 700,
                    length: 350,
                    height: 200,
                };
        }   

        if(!error){
        const truck = new Truck({
            created_by: _id,
            type,
            payload,
            demension
        })
        
        await truck
        .save()
        .then(ress => {
            console.log(ress)
            res.status(200).json({
                message: "Truck created successfully"
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                message: err.message
            })
        })
        }
    } catch (err) {
        console.log(err)
        res.status(500).json({
            message: err.message
        })
    }
}

const getTruckById = async (req, res, next) => {
    try {
        const { _id, role } = req.currentProfile;
        const { id } = req.params;

        if(role.toLowerCase() === 'shipper'){
            res.status(400).json({
                message: "Shipper can't have trucks"
            })
        }

        const findTruckById = await Truck.findOne({ _id: id, created_by: _id}).select(['-__v', '-demension', '-payload'])

        if(!findTruckById){
            res.status(400).json({
                message: "Truck with this ID does not exist"
            })
        }

        res.status(200).json({ findTruckById })

    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}

const updateTruckById = async (req, res, next) => {
    try {
        const { error } = truckValidation(req.body);
        const { _id, role } = req.currentProfile;
        const { id } = req.params;
        const { type } = req.body;

        if( error ) {
            res.status(403).json({
                message: error.details[0].message
            })
        }

        if(role.toLowerCase() === 'shipper'){
            res.status(400).json({
                message: "Shipper can't have trucks"
            })
        }

        const findTruckById = await Truck.findOne({ _id: id, created_by: _id })

        if(!findTruckById){
            res.status(400).json({
                message: "Truck with this ID does not exist"
            })
        }

        if(!findTruckById.status === 'OL') {
            res.status(400).json({
                message: "You can not update truck with On Load status"
            })
        }

        if(!findTruckById.created_by === !findTruckById.assigned_to){
            res.status(400).json({
                message: "You can not update assigned to you trucks"
            })
        }
        if(!error){
        await Truck.findOneAndUpdate({ created_by: _id, _id: id}, { type })
        res.status(200).json({
            message: "Truck details changed successfully"
        })
    }
    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}

const deleteTruck = async (req, res, next) => {
    try {
        const { _id, role } = req.currentProfile;
        const { id } = req.params;

        const findTruckById = await Truck.findOne({ _id: id, created_by: _id })

        if(role.toLowerCase() === 'shipper'){
            res.status(400).json({
                message: "Shipper can't have trucks"
            })
        }

        if(!findTruckById){
            res.status(400).json({
                message: "Truck with this ID does not exist"
            })
        }

        if(!findTruckById.status === 'OL') {
            res.status(400).json({
                message: "You can not update truck with On Load status"
            })
        }

        if(!findTruckById.created_by === !findTruckById.assigned_to){
            res.status(400).json({
                message: "You can not update assigned to you trucks"
            })
        }

        await Truck.findOneAndDelete({ created_by: _id, _id: id })
        res.status(200).json({
            message: "Truck deleted successfully"
        })
    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}


const assignedTruck = async (req, res, next) => {
    try {
        const { _id, role } = req.currentProfile;
        const { id } = req.params;

        const findTruckById = await Truck.findOne({ _id: id, created_by: _id })

        if(role.toLowerCase() === 'shipper'){
            res.status(400).json({
                message: "Shipper can't have trucks"
            })
        }

        if(!findTruckById){
            res.status(400).json({
                message: "Truck with this ID does not exist"
            })
        }

        if(!findTruckById.status === 'OL') {
            res.status(400).json({
                message: "You can not update truck with On Load status"
            })
        }

        if(!findTruckById.created_by === !findTruckById.assigned_to){
            res.status(400).json({
                message: "You can not update assigned to you trucks"
            })
        }

        await Truck.findOneAndUpdate({ _id: id, created_by: _id }, { assigned_to: _id });

        res.status(200).json({
            message: "Truck assigned successfully"
        })
    } catch (err) {
        res.status(500).json({
            message: err.message
        })
    }
}

module.exports = {
    getTrucks,
    createTruck,
    getTruckById,
    updateTruckById,
    deleteTruck,
    assignedTruck
}
const bcrypt = require('bcryptjs');
const { User, passwordValidation } = require('../models/userModel');




const {parsed: {ENCODE_ROUNDS, SECRET_KEY}} = require('dotenv').config({path: './.env'});


const getUserInfo = (req, res, next) => {
    try{
        return res.status(200).json({
            user: {
                _id: req.currentProfile._id,
                role: req.currentProfile.role,
                email: req.currentProfile.email,
                createDate: req.currentProfile.createDate
            }
        })
    } catch (err) {
        console.log(err)
        return res.status(500).json({
            message: err.message
        })
    }
}

const deleteUserInfo = async (req, res, next) => {
    try{   
        const deleteInfo = await User.findByIdAndDelete(req.currentProfile._id);

        if(!deleteInfo){
            return res.status(400).json({message: 'Current profile was already deleted'})
        }
        return res.status(200).json({message: 'Your profile has been deleted'})

    } catch (err) {
        return res.status(500).json({
            message: err.message
        })
    }
}

const updatePassword = async (req, res, next) => {
    const { oldPassword, newPassword } = req.body;
    const userProfile = await User.findById(req.currentProfile._id);


    if(!bcrypt.compareSync(oldPassword, userProfile.password)){
        return res.status(400).json({
            message: 'Your old password isnt correct'
        })
    }

    if(!newPassword){
        return res.status(400).json({
            message: 'New password cant be null'
        })
    }

    try{
            const { error } = passwordValidation(req.body)

            if(error){
                res.status(400).json({
                    message: error.details[0].message
                })
            }

        const encodePass = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(+ENCODE_ROUNDS))
        userProfile.password = encodePass;
        userProfile.save();

        return res.status(200).json({
            message: 'Success, you are changed a password'
        })
    } catch (err) {
        console.log(err)
        return res.status(500).json({
            message: err.message
        })
    }

}



module.exports = {
    getUserInfo,
    deleteUserInfo,
    updatePassword,
}
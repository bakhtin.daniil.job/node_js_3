const { Schema, model } = require('mongoose');
const Joi = require('joi');


const loadSchema = new Schema({
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    assigned_to: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    status: {
        type: String,
        enum: {
            values: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
            message: '{VALUE} is not supported'
        },
        default: 'NEW'
    },
    state: {
        type: String,
        enum: {
            values: [
                'Load is waiting to be assigned...',
                'En route to Pick Up',
                'Arrived to Pick Up',
                'En route to delivery',
                'Arrived to delivery'
            ],
            message: '{VALUE} is not supported'
        },
        default: 'Load is waiting to be assigned...'
    },
    name: {
        type: String,
        require: [true, 'Provide a name']
    },
    payload: {
        type: String,
        require: [true, 'Provide a payload']
    },
    pickup_address: {
        type: String,
        require: [true, 'Provide a pickup address']
    },
    delivery_address: {
        type: String,
        require: [true, 'Provide a delivery address']
    },
    dimensions: {
        width: {
            type: Number,
            require: [true, 'Provide a width of the load']
        },
        length: {
            type: Number,
            require: [true, 'Provide a length of the load']
        },
        height: {
            type: Number,
            require: [true, 'Provide a height of the load']
        }
    },
    logs: {
        type: Array,
        message: String,
        time: String,
        default: {
            message: 'Load is waiting to be assigned',
            time: new Date().toISOString()
        }
    },
    createDate: {
        type: String,
        default: new Date().toISOString()
    }
})


const Load = model('loads', loadSchema);

const loadValidation = load => {
    const schema = Joi.object({
        name: Joi.string().min(3).required(),
        status: Joi.string().valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'),
        state: Joi.string().valid(
        'Load is waiting to be assigned...',
        'En route to Pick Up',
        'Arrived to Pick Up',
        'En route to delivery',
        'Arrived to delivery'
        ),
        payload: Joi.number().required(),
        pickup_address: Joi.string().min(5).required(),
        delivery_address: Joi.string().min(5).required(),
        dimensions: Joi.object({
        width: Joi.number().required(),
        length: Joi.number().required(),
        height: Joi.number().required(),
        })
    })

    return schema.validate(load)
}



module.exports = {
    Load,
    loadValidation
}
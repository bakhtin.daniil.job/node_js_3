const { Schema, model } = require('mongoose');
const Joi = require('joi');


const userSchema = new Schema({
    email: {
        type: String,
        require: true,
        unique: true,
        index: true
    },
    password: {
        type: String,
        require: true
    },
    role: {
        type: String,
        enum: {
            values: ['DRIVER', 'SHIPPER'],
            message: "{VALUE} is not supported"
        },
        require: true
    },
    createDate: {
        type: String,
        default: new Date().toISOString()
    }
})

const User = model('users', userSchema);

const userValidate = user => {
    const schema = Joi.object({
        email: Joi.string().min(3).required().email(),
        password: Joi.string().min(4).max(20).required(),
        role: Joi.string().valid('DRIVER', 'SHIPPER')
    })

    return schema.validate(user)
}

const loginValidation = user => {
    const schema = Joi.object({
        email: Joi.string().min(3).required().email(),
        password: Joi.string().min(4).max(20).required()
    })

    return schema.validate(user)
}

const passwordValidation = user => {
    const schema = Joi.object({
        oldPassword: Joi.string().min(4).max(20).required(),
        newPassword: Joi.string().min(4).max(20).required()
    })

    return schema.validate(user)
}

const emailValidation = user => {
    const schema = Joi.object({
        email: Joi.string().min(3).required().email()
    })

    return schema.validate(user)
}
module.exports = {
    User,
    userValidate,
    loginValidation,
    passwordValidation,
    emailValidation
}
const { Schema, model } = require('mongoose');
const Joi = require('joi');


const truckSchema = new Schema({
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        require: true
    },
    assigned_to: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null,
        require: true
    },
    type: {
        type: String,
        require: true
    },
    status: {
        type: String,
        default: 'IS',
        require: true
    },
    payload: {
        type: Number
    },
    demension: {
        width: {
            type: Number
        },
        length: {
            type: Number
        },
        height: {
            type: Number
        }
    },
    createDate: {
        type: String,
        default: new Date().toISOString()
    }
})

const Truck = model("trucks", truckSchema);

const truckValidation = truck => {
    const schema = Joi.object({
        type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
    })

    return schema.validate(truck)
}

module.exports = {
    Truck,
    truckValidation
}
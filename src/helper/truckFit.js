const getTruckDimensions = (truckType) => {
    switch (truckType) {
      case "SPRINTER":
        return {
          width: 300,
          length: 250,
          height: 170
        };
      case "SMALL STRAIGHT":
        return {
          width: 500,
          length: 250,
          height: 170
        };
      case "LARGE STRAIGHT":
        return {
          width: 700,
          length: 350,
          height: 200
        };
      default:
        throw new Error(`Truck type ${truckType} is not correct`);
    }
  };
  
  const getTruckPayload = (truckType) => {
    switch (truckType) {
      case "SPRINTER":
        return 1700;
      case "SMALL STRAIGHT":
        return 2500;
      case "LARGE STRAIGHT":
        return 4000;
      default:
        throw new Error(`Truck type ${truckType} is not correct`);
    }
  };
  
  const loadIsFit = (truckType, loadDimensions, loadPayload) => {
    const truckDimensions = getTruckDimensions(truckType);
    const truckPayload = getTruckPayload(truckType);
  
    if (
      truckPayload >= loadPayload &&
      truckDimensions.width >= loadDimensions.width &&
      truckDimensions.length >= loadDimensions.length &&
      truckDimensions.height >= loadDimensions.height
    )
      return true;
  
    return false;
  };
  
  module.exports = {
    loadIsFit,
    getTruckDimensions,
    getTruckPayload
  };
  